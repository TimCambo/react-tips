/**
 * function classique Vs function flechées
 */
//function classique
function foo() {
    console.log('test');
}
foo();

//function flechés
const add = (a, b) => {
    return a + b;
};
console.log(add(2,2));
//avec juste un return dans une fonction fleché on peut écrire
const add2 = (a, b) => a + b;
console.log(add2(2,3));

/**
 * PPté this
 */
const myObj = {
    game:5,
    a: function () {
        /**
         * Context appelant
        * donc celui qui appel la methode (function dans un objet) a()
        * il s'agit de l'objet myObj
        */
        console.log(this);
        //exemple : j peut être appeler ici
        console.log(this.game);
    },
    /**
     * this dans fonction fleché
     * this affiche le context englobant donc l'objet window
     */

    fleche: () => {
        console.log(this)
    },
    //si c'est une fonction classique dans un objet
    // alors on peut l'écrire de cette façon
    ab() {
        console.log('Hello');
    }

};
myObj.a()
myObj.ab()