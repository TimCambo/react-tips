/**
 * Scoope
 * var est function's scoop: atteignable dans toutes la function
 * let et const sont block scoop: loggable que si la fonction passe dans la condition
 */

function foo() {
    if (true) {
        var x = 5;
    }
    console.log(x);
}
foo();

//Redéclaration: que avec var (let et const donne une erreur, plus sur contre les bugs)
var x = 5;
var x = 10;

//Hoisting: les appel de function peuvent se faire avant car
// le JS remonte les functions au début du fichier
test();
function test() {
    var x = 5;
    console.log(x);
}

// diif entre var et const c'est qu'on ne peut pas changer la valeur de la const
// diff entre var et let/const c'est que c'est une global (visible dans l'objet window)
let c = {a: 7};
c.a = 4;
console.log(c);