/**
 * spread operator
 * par exemple pour un tableau cela permet de voir les valeurs sans les appeler une par une
 * représenté par '...'
 */
const arr = [0, 1, 2];
const arr2 = [...arr, 4];
console.log(arr2);
/**
 * cas des objets
 */
const myObj = {
    a:1, b:1, c:1
}
const myObj2 = {
    ...myObj,
    f:1
}
console.log(myObj2);

/**
 * rest operator
 * ici args est sous form de tableau
 */
function foo(...args) {
    let result = 0
    //for of c'est le foreach pour tableau js
    for (const arg of args) {
        result += arg
    }
    return result;
}
console.log(foo(2,2));
